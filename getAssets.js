#!/usr/bin/env node

(() => {
	'use strict';

	let fs     = require('fs'),
		exec   = require('child_process').exec;


	const ASSETS_GLOBAL_FILE = 'assets_global.json';

	const DESTINATION = 'public';

	// Tried to define these values in package.json or .npmrc without success (maddening)
	const BO_SSH_HOST = 'ovh1';
	const BO_SSH_USER = 'www';
	const BO_SSH_PATH = '~/siae-prod.mobile-spot.mobi/current/public';

	const CON_STRING = `${BO_SSH_USER}@${BO_SSH_HOST}:${BO_SSH_PATH}`;


	/**
	 * Wrapper to manage downloads
	 */
	let Downloader = (() => {
		let queue = [],
			currentIndex = 0,
			running = 0,
			maxSimultaneous = 5;

		/**
		 * Add an entry to download
		 * @param  {object} value
		 */
		let push = (value) => {
			queue.push(value);
			_proceed();
		};

		let _proceed = () => {
			if (queue.length > currentIndex && running < maxSimultaneous) {
				_download(queue[currentIndex++]);
			}
		};

		/**
		 * Download a file from the server
		 * @param {string} options.path
		 * @param {string} options.dest
		 * @param {function} options.next (optional)
		 */
		let _download = (/*{ path, dest, next } needs babel*/ ref) => {
			let path = ref.path,
				dest = ref.dest,
				next = ref.next;

			running++;
			let startTime = new Date().getTime(),
				indexLabel = currentIndex+'/'+queue.length+' ';

			// Perform download via scp
			let performScp = () => {
				exec('scp "' + CON_STRING + path + '" "' + dest + '"', (err) => {
					running--;
					if (err) {
						console.error(indexLabel+'Failed to download '+path, err);
					} else {
						console.log(indexLabel+'['+(new Date().getTime()-startTime)+'ms] Succesfully downloaded ' + path);
						if (typeof next === 'function') {
							next();
						}
						_proceed();
					}
				});
			};

			if (dest.indexOf('/') === -1) {
				performScp();
			} else {
				// Create destination directory
				exec('mkdir -p '+dest.substr(0, dest.lastIndexOf('/')), (err) => {
					if (err) {
						console.error('Failed to create directory', err);
					} else {
						performScp();
					}
				});
			}
		};

		let reset = () => {
			queue.length = 0;
			currentIndex = 0;
		};

		return {
			push: push,
			reset: reset,
		};
	})();


	/**
	 * Parse and iterate on `ASSETS_GLOBAL_FILE` content
	 */
	let readAssetsGlobal = () => {
		console.log('Parse content of ' + ASSETS_GLOBAL_FILE);
		Downloader.reset();

		// Download listed content in "all"
		iterateOn(require(`./${DESTINATION}/${ASSETS_GLOBAL_FILE}`).all);
	};


	/**
	 * Iterate on entries to download
	 * @param  {object} obj
	 */
	let iterateOn = (obj) => {
		console.log('Iterate on content');
		for (let path in obj) {
			Downloader.push({
				path: path,
				dest: DESTINATION + path,
			});
		}
	};


	// Start
	Downloader.push({
		path: '/js/' + ASSETS_GLOBAL_FILE,
		dest: DESTINATION,
		next: readAssetsGlobal
	});
})();