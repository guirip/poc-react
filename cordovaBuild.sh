#!/bin/sh
# GR 10/11/2016

bold="\033[1m"
normal="\033[0m"

buildDir=cordova-poc-app
appId="com.mobilespot.pocReact"
appName="POC-React"
runTarget=device

log()
{
	echo
	echo $bold$@$normal
}

if [ $# -lt 1 ]
then
	log "Missing platform argument [android/ios]"
	exit 1
fi
os=$1

if [ -d "${buildDir}" ]
then
	log "Remove existing directory: ${buildDir}"
	rm -Rf $buildDir
fi


log "Build project for production" && \
npm run build && \


log "Create cordova project ["$appId"] ["$appName"]" && \
cordova create $buildDir $appId $appName && \
cd $buildDir && \


log "Clean www/ directory and copy poc app" && \
rm -Rf www && \
cp -RL ../public www && \
cd www && \
mkdir build && \
cp -RL ../../build/static/js/*.js* build/ && \
cp -RL ../../build/static/css/*.css* build/ && \


log "Inject script/link tags in index.html for generated js & css" && \
cd build && \
jsFilename=$(ls *.js) && \
cssFilename=$(ls *.css) && \
cd - > /dev/null && \
cat index.html | \
	sed -e "s/<\/body>/<script type=\"text\/javascript\" src=\"build\/${jsFilename}\"><\/script><\/body>/" | \
	sed -e "s/<\/head>/<link href=\"build\/${cssFilename}\" rel=\"stylesheet\"><\/head>/" | \
	sed -e "s/%PUBLIC_URL%\///" > index.html.tmp && \
rm -f index.html && \
mv index.html.tmp index.html && \


log "Add Cordova plugins" && \
cordova plugin add cordova-plugin-file && \
cordova plugin add cordova-plugin-file-transfer && \
# for easier debugging on android < 5
cordova plugin add cordova-plugin-crosswalk-webview && \


log "Build app" && \
cordova platform add $os && \
cordova build && \


log "Run app on ${runTarget}" && \
cordova run --$runTarget


status=$?
if [ $? -gt 0 ]
then
	log "Failure"
fi
echo
exit $status
