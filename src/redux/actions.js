
export const UPDATE_FROM_DB = 'UPDATE_FROM_DB';

export const updateFromDb = (tables) => {
	return {
		type  : UPDATE_FROM_DB,
		tables: tables,
	}
};


export const SET_EXHIBITOR_ID = 'SET_EXHIBITOR_ID';

export const setExhibitorId = (id) => {
	return {
		type: SET_EXHIBITOR_ID,
		id  : id,
	};
};



export const SET_LIST_TYPE = 'SET_LIST_TYPE';

export const setListType = (type) => {
	return {
		type    : SET_LIST_TYPE,
		dataType: type,
	};
};


