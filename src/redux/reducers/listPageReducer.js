
import { UPDATE_FROM_DB, SET_LIST_TYPE } from '../actions';

import Db from '../../core/oldstuff/Db';
import { objectToArray } from '../../core/util/JsTools';

let Immutable = require('immutable');

const LOG_PREF = '[listPagePageReducer] ';


const getListData = (type) => {
	let table = Db.getTableNameFromType(type);
	return Immutable.List(Db.data && Db.data[table] ? objectToArray(Db.data[table].data) : []);
};

const hasATableBeenUpdated = (type, updatedTables) => {
	return updatedTables === null || updatedTables.indexOf(Db.getTableNameFromType(type)) !== -1;
};

export default (state = {}, action) => {
	console.log('reducer listPageReducer, action:', action);
	switch (action.type) {

		case UPDATE_FROM_DB:
			if (state.type === null || typeof state.type === 'undefined') {
				// if type is not defined, page has not been displayed yet
			}
			else if (hasATableBeenUpdated(state.type, action.tables) !== true) {
				// no need to fetch fresh data
				console.log(LOG_PREF+'Data update has no impact');
			}
			else {
				console.log(LOG_PREF+'Fetch updated data');
				return Object.assign({}, state, {
					items: getListData(state.type),
				});
			}
			break;

		case SET_LIST_TYPE:
			return Object.assign({}, state, {
				items: getListData(action.dataType),
				type : action.dataType,
			});

		default:
	}
	return state;
};