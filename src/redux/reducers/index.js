//import { combineReducers } from 'redux'
import exhibitorPageReducer from './exhibitorPageReducer';
import listPageReducer      from './listPageReducer';

import Pages from '../../pages/Pages';

const app = (state = {}, action) => {
	let reducers = {};

	reducers[Pages.List.key] = listPageReducer(state[Pages.List.key], action);
	reducers[Pages.Exhibitor.key] = exhibitorPageReducer(state[Pages.Exhibitor.key], action);

	return reducers;
};

export default app