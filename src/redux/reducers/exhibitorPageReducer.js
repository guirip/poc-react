
import { UPDATE_FROM_DB, SET_EXHIBITOR_ID } from '../actions';

import Db from '../../core/oldstuff/Db';

//let Immutable = require('immutable');

const LOG_PREF = '[exhibitorPageReducer] ';

const USED_TABLES = ['exhibitors', 'countries'];


const getExhibitorData = (id) => {
	// TODO: use Immutable.js
	return Db.data && Db.data.exhibitors ? Db.data.exhibitors.data[id] : null;
};

const hasATableBeenUpdated = (updatedTables) => {

	// if value is null, we safely choose to consider that data could have been updated
	let found = updatedTables === null ? true : false;

	for (let i=0; found === false && i<updatedTables.length; i++) {
		found = USED_TABLES.indexOf(updatedTables[i]) !== -1;
	}
	return found;
};


export default (state = {}, action) => {
	console.log(LOG_PREF+'action:', action);

	switch (action.type) {

		case SET_EXHIBITOR_ID:
			return Object.assign({}, state, {
				id: action.id,
				exhibitor: getExhibitorData(action.id),
			});

		case UPDATE_FROM_DB:
			if (state.id === null || typeof state.id === 'undefined') {
				// if exhibitor id is not defined, exhibitor page has not been displayed yet
			}
			else if (hasATableBeenUpdated(action.tables) !== true) {
				// no need to fetch fresh data
				console.log(LOG_PREF+'Data update has no impact');
			}
			else {
				console.log(LOG_PREF+'Fetch updated data');
				return Object.assign({}, state, {
					exhibitor: getExhibitorData(state.id),
				});
			}
			break;

		default:
	}
	return state;
};
