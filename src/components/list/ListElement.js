import React, { PropTypes } from 'react'

/**
 * Stateless ListElement component
 */
const ListElement = ({ text, textMinor, ...attrs }) => (

	<li {...attrs}>{text} <small>{textMinor ? ' - '+textMinor : ''}</small></li>
)

ListElement.propTypes = {
	onClick  : PropTypes.func.isRequired,
	text     : PropTypes.string.isRequired,
}

export default ListElement