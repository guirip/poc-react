import React, { PropTypes } from 'react';

import './List.css';
import ListLoaderElement from './ListLoaderElement';
import ListElement       from './ListElement';


//const LOG_PREF = '[Comp.List] ';


/**
 * Stateless List component
 */
const List = (props) => {

	let listElements;
	if (!props.items || !props.items.size) {
		listElements = <ListLoaderElement />;
	}
	else {
		listElements = props.items.map((item, index) => {
			let attributes = {
				key: index,
			};
			for (let key in props.helpers) {
				if (props.helpers.hasOwnProperty(key) === true) {
					attributes[key] = props.helpers[key](item);
				}
			}
			return <ListElement key={index} {...attributes} />
		});
	}
	return (
		<ul className="list-component">
			{ listElements }
		</ul>
	);
}

List.propTypes = {
	items  : PropTypes.object.isRequired,
	type   : PropTypes.string.isRequired,
	helpers: PropTypes.object.isRequired,
};

export default List;
