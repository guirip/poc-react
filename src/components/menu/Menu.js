import React, { PropTypes } from 'react';

import { Splitter, SplitterSide, SplitterContent, Page, List, ListItem } from 'react-onsenui';

import './Menu.css';

import Router from '../../core/Router';
import Db     from '../../core/oldstuff/Db';
import Updater     from '../../core/oldstuff/Updater';
import Pages  from '../../pages/Pages';

const LOG_PREF = '[Comp.Menu] ';


let toggle = (pageKey) => {
	if (!pageKey) {
		console.error(LOG_PREF+'Missing `pageKey` parameter');
		return;
	}

	// Onsen add functions to DOM elements
	Pages[pageKey].getElement().querySelector('ons-splitter-side').toggle();
};
export { toggle as toggleMenu };

/**
 * @param {object} options
 * @param {string} associatedPageKey: page this menu is attached to
 * @param {Components} children
 */
const Menu = ({options, associatedPageKey, children}) => {

	let defautOptions = {};
	defautOptions[Pages.List.key] = { type: 'Service' };


	// Pages
	let handlePageSelected = (pageKeyToGoTo) => {
		toggle(associatedPageKey);
		Router.go(pageKeyToGoTo, defautOptions[pageKeyToGoTo]);
	};
	let items = Object.keys(Pages).filter((pageKey) => Pages[pageKey].inMenu !== false).map((pageKey, index) => {
		let isEnabled = associatedPageKey !== pageKey;
		return (
			<ListItem key={index} className={isEnabled ? '' : 'menu-element-disabled'} tappable onClick={isEnabled ? () => handlePageSelected(pageKey) : null}>{Pages[pageKey].label}</ListItem>
		);
	});


	// Refresh data button
	let handleRefresh = () => {
		toggle(associatedPageKey);
		(typeof cordova !== 'undefined' ? Updater.startUpdate : Db.refresh)();
	};
	items.push(<ListItem key={99} tappable onClick={handleRefresh}>Refresh data</ListItem>);


	return (
		<Splitter>
			<SplitterSide width="220px" collapse side="right" isSwipeable={!options || options.swipeable !== false}>
				<Page>
					<List>
						{ items }
					</List>
				</Page>
			</SplitterSide>
			<SplitterContent>
				{ children }
			</SplitterContent>
		</Splitter>
	);

};
Menu.propTypes = {
	// Indicate on which page the menu instance is attached
	associatedPageKey: PropTypes.string.isRequired,
}
export default Menu;

