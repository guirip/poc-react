/* global menu:false */

import React from 'react';

import { Page, Toolbar, ToolbarButton, Icon, BackButton } from 'react-onsenui';

import { toggleMenu } from '../menu/Menu';
import Router from '../../core/Router';


const Skeleton = ({title, pageKey, children}) => {

	const goBack = (event) => {
		Router.goBack();
	};

	const renderToolbar = () => (
		<Toolbar>
			<div className="left"><BackButton onClick={goBack}></BackButton></div>
			<div className="center">{title}</div>
			<div className="right">
				<ToolbarButton onClick={() => toggleMenu(pageKey)}>
					<Icon icon="md-menu" size={{default: 28}}></Icon>
				</ToolbarButton>
			</div>
		</Toolbar>
	);

	return (
		<Page renderToolbar={renderToolbar}>
			{ children }
		</Page>
	);
};

Skeleton.propTypes = {
	title: React.PropTypes.string.isRequired
};

export default Skeleton;