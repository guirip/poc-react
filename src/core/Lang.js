
const SUPPORTED_LANG = ['fr', 'en'];

export default {
	let lang = window.navigator.userLanguage || window.navigator.language;

	return {
		set lang(value) {
			if (SUPPORTED_LANG.indexOf(value) === -1) {
				console.error('Unsupported language ' + value);
			} else {
				lang = value;
			}
		},
		get lang() {
			return lang;
		},
	};
};