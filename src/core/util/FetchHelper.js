
import ons from 'onsenui';

const LOG_PREF = '[FetchHelper] ';

let FETCH_ERRORS = {
	REQUEST: 'Request failed',
	PARSE  : 'Failed to parse json',
};

/**
 * Common error handler
 * @param {string}   msg
 * @param {function} cb
 * @param {...[*]}   args
 */
let errorHandler = (msg, cb, ...args) => {
	console.error(LOG_PREF+msg, args);

	// UI display error
	ons.notification.alert({
		title: 'Error',
		message: msg ? msg : 'Failed to retrieve data',
	});

	if (typeof cb === 'function') {
		cb();
	}
};

/**
 * Handle non-JSON requests
 * @param {object} promise
 * @param {function} successCb (optional)
 * @param {function} errorCb   (optional)
 */
let commonHandler = (promise, successCb, errorCb) => {
	promise.then(
		(response) => {
			if (response.status >= 400) {
				errorHandler(FETCH_ERRORS.REQUEST + ` (${response.status})`, errorCb);
			} else {
				console.debug(LOG_PREF+'Loaded ' + response.url);
				if (typeof successCb === 'function') {
					successCb(response);
				}
			}
		},
		// fetch failure
		(...args) => {
			errorHandler(FETCH_ERRORS.REQUEST, errorCb, args);
		});
};

/**
 * Handle JSON requests
 * @param {object} promise
 * @param {function} successCb (optional)
 * @param {function} errorCb   (optional)
 */
let jsonHandler = (promise, successCb, errorCb) => {
	promise
		.then(
			(response) => {
				if (response.status >= 400) {
					errorHandler(FETCH_ERRORS.REQUEST + ` (${response.status})`, errorCb);
				} else {
					console.debug(LOG_PREF+'Loaded ' + response.url);
					return response.json();
				}
			},
			// fetch failure
			(...args) => {
				errorHandler(FETCH_ERRORS.REQUEST, errorCb, args);
			})
		.then(
			// parse success
			(json) => {
				if (json) {
					if (typeof successCb === 'function') {
						successCb(json);
					}
				} else {
					errorHandler(FETCH_ERRORS.PARSE, errorCb);
				}
			},
			// parse failure
			(...args) => {
				errorHandler(FETCH_ERRORS.PARSE, errorCb, args);
			});
};

/**
 * Handle response for relative path in cordova app, and file:// protocol
 * @param  {string} url
 */
let fetchLocal = (url) => {
	return new Promise(function(resolve, reject) {
		var xhr = new XMLHttpRequest
		xhr.onload = function() {
			resolve(new Response(xhr.responseText, { status: xhr.status }))
		}
		xhr.onerror = function() {
			reject(new TypeError('Local request failed'))
		}
		xhr.open('GET', url)
		xhr.send(null)
	})
}

/**
 * @param {string} url
 * @param {object} opt (optional)
 * @param {function} successCb (optional)
 * @param {function} errorCb   (optional)
 */
export default (url, opt, successCb, errorCb) => {
	let promise,
		isJson = true;

	if ((typeof cordova !== 'undefined' && url.indexOf('://') === -1) || url.slice(0, 7) === 'file://') {
		// Fetch polyfill does not handle file:// protocol
		promise = fetchLocal(url);

		// Only local resource with json extension can be json
		if (url.indexOf('.json') === -1) {
			isJson = false;
		}

	} else {
		// Default (web-service, rest api, etc) is supposed to be json
		promise = fetch(url, opt);
	}

	if (isJson) {
		jsonHandler(promise, successCb, errorCb);
	} else {
		commonHandler(promise, successCb, errorCb);
	}
};
