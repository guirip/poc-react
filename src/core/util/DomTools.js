
const LOG_PREF = '[DomTools] ';

let cssSnakeCaseStylePattern = /-./;

/**
 * Camelize a css property name.
 * ex: -webkit-transform-origin -> webkitTransformOrigin
 * ex: background-color -> backgroundColor
 * @param  {string} str
 * @return {string}
 */
let camelizeCssPropertyName = (str) => {
	let match, to;
	do {
		match = cssSnakeCaseStylePattern.exec(str);
		if (match && match.length) {
			// Camelize only if not at the beginning of the property (ex: -webkit...)
			to = match[0].charAt(1);
			if (match.index > 0) {
				to = to.toUpperCase();
			}
			str = str.replace(match[0], to);
		} else {
			break;
		}
	} while (true);
	return str;
};

/**
 * Apply a bunch of style properties to a DOM element
 * @param {DOM element} element
 * @param {object} style
 * @param {DOM element} the updated element
 */
let applyStyle = (element, style) => {

	if (!element){
		console.error(LOG_PREF+'Missing argument `element`');
		return;
	}
	if (!style){
		console.error(LOG_PREF+'Missing argument `style`');
		return;
	}
	Object.keys(style).forEach((key) => {
		// Apply property
		element.style[camelizeCssPropertyName(key)] = style[key];
	});
	return element;
};

export { applyStyle };