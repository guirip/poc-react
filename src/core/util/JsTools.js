
/**
 * Return an array from an object
 * @param	{object} obj
 * @return {array}
 */
let objectToArray = (obj) => {
	return Object.keys(obj).map((key) => obj[key]);
}
export { objectToArray };


/**
 * Return a querystring from object properties
 * @see http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object
 * @param  {object} obj
 * @return {string}
 */
let serialize = (obj) => {
	let str = [];
	for(let p in obj)
		if (obj.hasOwnProperty(p)) {
			str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
		}
	return str.join('&');
}
export { serialize };

