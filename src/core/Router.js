import ReactDOM      from 'react-dom';
import React         from 'react';
// import { createHistory } from 'history'

import { SEPARATOR, DEFAULT_PAGE }      from './Config';
import Pages, { PAGE_CSSCLASS }         from '../pages/Pages';
import { perform as performTransition } from './transition/PageTransition';
import { TYPE as TRANSITION_TYPE }      from './transition/TransitionConfig';
import { serialize }                    from './util/JsTools';


const LOG_PREF = '[Router] ';


// HTML5 History polyfill
// let history = createHistory();
if (history.emulate) {
	console.log(LOG_PREF+'Polyfill in action for HTML5-History-API');
} else {
	console.log(LOG_PREF+'Native support for HTML5-History-API');
}
// let location = history.location;
let location = window.history.location || window.location;


let currentPageKey,
	historyIndex  = 0,
	rootContainer = document.getElementById('root'),
	reduxStore;


/**
 * Mount a react component in a dedicated DOM container
 * @param {string} pageKey (@see Pages object)
 * @param {object} options (component props)
 */
let mountPage = (pageKey, options) => {

	// Check arguments
	let hasError = false;
	if (!pageKey) {
		console.error(LOG_PREF+'Missing argument `pageKey`');
		hasError = true;
	}
	else if (!Pages[pageKey]) {
		console.error(LOG_PREF+'Invalid argument `pageKey`', pageKey);
		hasError = true;
	}
	if (hasError) {
		return;
	}

	let page = Pages[pageKey];
	if (!page.isMounted || page.mountOnce !== true) {

		let pageEl = page.getElement();
		if (!pageEl) {
			// Create DOM container
			pageEl = document.createElement('div');
			pageEl.id = page.elId;
			pageEl.classList.add(PAGE_CSSCLASS);
			rootContainer.appendChild(pageEl);

			page.setElement(pageEl);
		}

		// Mount react component
		page.instance = ReactDOM.render(
			React.createElement(page.component, Object.assign({}, options, { store: reduxStore })),
			pageEl,
		);
		page.isMounted = true;
	}
};


/**
 * Transmit options to component using an event
 * @param {object} page
 * @param {object} options
 */
let transmitOptions = (page, options) => {
	if (page.events.updOptions &&
			page.instance &&
			typeof page.instance.onEvent === 'function') {

		let _transmitOptions = () => {
			let evt = new Event(page.events.updOptions);
			evt.data = options;
			page.instance.onEvent(evt);
		};
		window.requestAnimationFrame(_transmitOptions);

	}
};


/**
 * Navigate to a page
 * @param {object} page (@see Pages object)
 * @param {object} options
 * @param {TRANSITION_TYPE} overrideTransition
 * @param {boolean} pushToHistory
 */
let go = (page, options, overrideTransition, pushToHistory) => {

	let _page,
		hasOptions = options !== null &&
					 typeof options === 'object' &&
					 Object.keys(options).length > 0;

	// Check page argument for empty value
	if (!page) {
		console.error(LOG_PREF+'Missing argument `page`');
		return;
	}
	// check key
	else if (typeof page === 'string') {
		_page = Pages[page];
		if (typeof _page === 'undefined') {
			console.error(LOG_PREF+'Invalid argument `page`', page);
			return;
		}
	}
	// object = we assume it's a proper page definition
	else {
		_page = page;
	}
	console.log(LOG_PREF+'go '+_page.key+', options:', options);


	// Mount demanded page
	mountPage(_page.key, options);

	// Change page
	if (_page.key !== currentPageKey) {

		let next,
			transitionType = TRANSITION_TYPE.initial,
			previousPage   = currentPageKey;

		if (previousPage) {
			transitionType = TRANSITION_TYPE.forward;
			next = () => {
				Pages[previousPage].active = false;
			};
		}

		// Show demanded page
		_page.active = true;
		currentPageKey = _page.key;

		performTransition(currentPageKey, previousPage, next, overrideTransition ? overrideTransition : transitionType);
	}

	// Update navigation status (url/history/title)
	if (pushToHistory !== false) {
		console.log(LOG_PREF+'pushToHistory', _page.path, options);

		let querystring = '?' + _page.path + (hasOptions ? '&'+serialize(options) : '');

		history.pushState({
			pageKey: _page.key,
			options: options,
			historyIndex: historyIndex++
		}, `${_page.key} page`, querystring);
	}
	document.title = _page.label;

	if (options !== null && typeof options === 'object') {
		transmitOptions(_page, options);
	}
};


/**
 * Called on initial page load, parse querystring to determine the page to display
 * @param {TRANSITION_TYPE} overrideTransition
 * @param {boolean} pushToHistory
 */
let applyCurrentUrl = (transition, pushToHistory) => {
	// Parse queryString to get the route (if any)
	let separatorIndex = location.href.indexOf(SEPARATOR),
		query = {},
		pageToDisplay;

	if (separatorIndex !== -1) {

		// Parse querystring to an object
		location.href.substring(separatorIndex+1).split('&').forEach((keyValue) => {
			let splitKeyValue = keyValue.split('=');
			query[splitKeyValue[0]] = splitKeyValue.length > 1 ? splitKeyValue[1] : null;
		});
		for (let qsKey in query) {
			if (query.hasOwnProperty(qsKey) === true) {
				if (/^\d*$/.test(query[qsKey]) === true) {
					query[qsKey] = parseInt(query[qsKey], 10);
				}
			}
		}

		for (let key in query) {
			if (key.slice(0,1) === '/'){
				pageToDisplay = Pages[Object.keys(Pages).filter((pageKey) => Pages[pageKey].path === key)];

				// Avoid transmitting an empty options object
				if (Object.keys(query).length === 1) {
					query = null;
				} else {
					delete query[key];
				}
				continue;
			}
		}
	}
	if (typeof pageToDisplay === 'undefined') {
		pageToDisplay = Pages[DEFAULT_PAGE];
	}
	go(pageToDisplay, query, transition, pushToHistory);
};


/**
 * Programmatically go to previous page
 */
let goBack = () => {
	history.go(-1);
};


/**
 * Handle when history is manipulated (going forward or backward)
 */
let lastHistoryIndex = null;
window.onpopstate = (e) => {

	// We define `state` when routing, so if state if not defined ignore event (no more routing history)
	if (e.state) {
		let transition = TRANSITION_TYPE.backward;

		if (lastHistoryIndex !== null && lastHistoryIndex < e.state.historyIndex) {
			transition = TRANSITION_TYPE.forward
		}
		lastHistoryIndex = e.state.historyIndex;
		applyCurrentUrl(transition, false);
	}
};


let publicApi = {
	setReduxStore    : (store) => { reduxStore = store; },
	go               : go,
	goBack           : goBack,
	applyCurrentUrl  : applyCurrentUrl,
	getCurrentPageKey: () => currentPageKey,
};
export default publicApi;


// TEMP DEV
window.Router = publicApi;
