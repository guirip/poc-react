

export const SEPARATOR = '?';
export const DEFAULT_PAGE = 'Home';

export const SUPPORTED_LANG = ['fr', 'en'];
let LANG;
let setLang = (value) => {
	if (SUPPORTED_LANG.indexOf(value) === -1) {
		console.error('Unsupported language ' + value);
	} else {
		LANG = value;
	}
};
setLang((window.navigator.userLanguage || window.navigator.language).slice(0,2));
export { LANG };


export const APP_VERSION = 1.0;

export const BO_URL = 'http://siae.mobi';
