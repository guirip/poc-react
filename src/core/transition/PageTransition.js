
import Pages           from '../../pages/Pages';
import { applyStyle }  from '../util/DomTools';
import { TRANSITION_DURATION as DURATION,
		 TYPE, START_STYLE, END_STYLE, RESET_STYLE} from './TransitionConfig';
import { performQueuedActions } from './TransitionQueuedActions';

const LOG_PREF = '[PageTransition] ';


let beforeTransition = (el) => {
	el.style.transition = `left ${DURATION/1000}s ease-out, top ${DURATION/1000}s ease-out, opacity ${DURATION/1000}s ease-out`;
	el.style.willChange = 'transform';
	el.style.zIndex     = 1;
};

let afterTransition = (el) => {
	el.style.transition = 'none';
	el.style.willChange = 'auto';
	el.style.zIndex     = 0;
};


let transitionActive = false;
let isTransitionRunning = () => transitionActive;

export { isTransitionRunning };



let perform = (pageKey, previousPageKey, callback, type) => {

	// Check arguments
	let hasError = false;
	if (!pageKey) {
		console.error(LOG_PREF + 'Missing transition argument `pageKey`');
		hasError = true;
	}
	else if (!Pages[pageKey]) {
		console.error(LOG_PREF + 'Invalid transition argument `pageKey`', pageKey);
		hasError = true;
	}

	if (previousPageKey && !Pages[previousPageKey]) {
		console.error(LOG_PREF + 'Invalid transition argument `previousPageKey`', previousPageKey);
		hasError = true;
	}

	if (typeof TYPE[type] === 'undefined') {
		console.error(LOG_PREF + 'Invalid transition argument `type`', type);
		hasError = true;
	}
	if (hasError) {
		return;
	}

	// Proceed
	let pageEl = Pages[pageKey].getElement();

	requestAnimationFrame(() => {
		// Set start properties
		afterTransition(pageEl);
		applyStyle(pageEl, START_STYLE[type]);

		requestAnimationFrame(() => {
			// Set destination properties
			beforeTransition(pageEl);
			transitionActive = true;
			applyStyle(pageEl, END_STYLE);


			// When translation is done:
			setTimeout(() => {
				requestAnimationFrame(() => {
					if (previousPageKey) {
						applyStyle(Pages[previousPageKey].getElement(), RESET_STYLE);
					}
					// remove transition property on current page
					afterTransition(pageEl);
					transitionActive = false;

					if (typeof callback === 'function') {
						callback();
					}

					// If actions have been queued while running the transition, execute them
					requestAnimationFrame(performQueuedActions);
				})
			}, DURATION);
		})
	});
};
export { perform };

