
import { isTransitionRunning } from './PageTransition';


const LOG_PREF = '[TransitionQueue] ';

let queuedActions = [];


/**
 * If a transition is running, then the callback argument will be executed once transition has completed.
 * If no transition is running, then the callback argument is executed immediately.
 * @param {Function} callback
 */
let performWhenAvailable = (callback) => {
	let hasError = false;
	if (!callback) {
		console.error(LOG_PREF + 'Missing argument `callback`');
		hasError = true;
	}
	else if (typeof callback !== 'function') {
		console.error(LOG_PREF + 'Invalid argument `callback`', callback);
		hasError = true;
	}
	if (hasError) {
		return;
	}

	if (isTransitionRunning()) {
		queuedActions.push(callback);
	} else {
		// Execute immediately if no transition is running
		callback();
	}
};
export { performWhenAvailable };


/**
 * [description]
 * @return {[type]} [description]
 */
let performQueuedActions = () => {
	//console.log(LOG_PREF + `Executing ${queuedActions.length} queued functions`);
	while (queuedActions.length) {
		queuedActions.shift()();
	}
};
export { performQueuedActions };


export default {
	performWhenAvailable: performWhenAvailable,
};
