

/**
 * @type {Number}
 */
export const TRANSITION_DURATION = 200; // milliseconds


/**
 * Various transitions
 * @type {Object}
 */
const TYPE = {
	initial : 'initial',
	forward : 'forward',
	backward: 'backward',
};
export { TYPE };


/**
 * Define start style for each transition type
 * @type {Object}
 */
let COMMON_START_STYLE = {
 	opacity: .4,
	display: 'block',
};
let START_STYLE = {};

START_STYLE[TYPE.initial] = Object.assign({
	left   : '0px',
	top    : '50px',
}, COMMON_START_STYLE);

START_STYLE[TYPE.forward] = Object.assign({
	left   : `${window.innerWidth}px`,
	top    : '0px',
}, COMMON_START_STYLE);

START_STYLE[TYPE.backward] = Object.assign({
	left   : `-${window.innerWidth}px`,
	top    : '0px',
}, COMMON_START_STYLE);

export { START_STYLE };


/**
 * End style, common for every transition
 * @type {Object}
 */
const END_STYLE = {
	left   : '0px',
	top    : '0px',
	opacity: 1,
};
export { END_STYLE };


/**
 * Reset previous page style (once transition has completed)
 * @type {Object}
 */
const RESET_STYLE = {
	zIndex : 0,
	display: 'none',
};
export { RESET_STYLE };
