
import fetchHelper from '../../core/util/FetchHelper';

import Oldies      from './Oldies';
import Phonegap    from './Phonegap';
import Updater     from './Updater';


const LOG_PREF = '[Assets] ';


export default () => {

	return (() => {


		let list = null,
			version = 1,
			listVersion = {};



		const save = () => {
			console.log(LOG_PREF+'save');
			window.localStorage.setItem('Assets.version', version);
			window.localStorage.setItem('Assets.listVersion', JSON.stringify(listVersion));
		};


		const saveList = () => {
			console.log(LOG_PREF+'Saving assets list into the APP LOCAL Storage: assets_list.json');
			const setFileContentWin = () => {
				console.log(LOG_PREF+'setFileContentWin');
			};
			const setFileContentFail = () => {
				console.log('setFileContentFail');
				console.error(LOG_PREF+'setFileContentFail error: Could not write APP LOCAL file: assets_list.json');
			};
			Phonegap.setFileContent('assets_list.json', JSON.stringify(list), setFileContentWin, setFileContentFail);
		};


		const loadFile = (callback) => {
			console.log(LOG_PREF+'loadFile (reset the assets with the REMOTE SERVER latest assets files)');

			fetchHelper(Oldies.getUrl(Updater.ASSETS_DEFINITION_FILE), null,
				// Success
				(datas) => {
					// MA: add checks on the format returned
					list = datas;
					version = 1;
					listVersion = {'initial': list.version};
					save();
					saveList();
					callback();
				},
				// Failure
				() => {
					console.error(LOG_PREF+'Error getting while fetching assets list json file. This means a failed build.');
				}
			);
		};


		const init = (callback) => {
			console.log(LOG_PREF+'init');

			version = parseInt(window.localStorage.getItem('Assets.version'), 10);
			console.log(LOG_PREF+"localStorage assets version: ", version);

			if (isNaN(version) === true) {
				console.log(LOG_PREF+'first time launch!');
				loadFile(callback);
			}
			else {
				console.log(LOG_PREF+'not first time launch! Now read the APP LOCAL assets definition');

				const getFileContentWin = (data) => {
					console.log(LOG_PREF+'getFileContentWin');
					console.log(LOG_PREF+'Load APP LOCAL assets.');
					if (typeof data === 'string') {
						data = JSON.parse(data);
					}
					list = data;
					listVersion = JSON.parse(window.localStorage.getItem('Assets.listVersion'));
					if (version > 1) {
						Phonegap.getDirectory('V' + (version - 1),
							// Success
							() => {
								console.log(LOG_PREF+'OK: LOCAL APP assets directory exist... Continue!');
								callback();
							},
							// Failure
							() => {
								console.error(LOG_PREF+'Should not come here.... Means the APP LOCAL folder does not exist anymore...', 'V' + (version - 1));
								loadFile(callback);
							}
						);
					} else {
						console.log(LOG_PREF+'OK: LOCAL APP assets directory V1 exist... Continue!');
						callback();
					}
				};
				const getFileContentFail = () => {
					console.log(LOG_PREF+'getFileContentFail');
					console.error(LOG_PREF+'Should not come here.... Means the APP LOCAL assets_list.json could not get read OR write.');
					loadFile(callback);
				};
				Phonegap.getFileContent('assets_list.json', getFileContentWin, getFileContentFail);
			}
		};


		const debug = () => {
			console.log('**********   ASSETS  ************');
			console.log('----------- VARIABLES -----------');
			console.log('version: ', version);
			console.log('listVersion: ', listVersion);
			console.log('list: ', list);
			console.log('');
			console.log('----------- LOCALSTORAGE -----------');
			console.log('version:', window.localStorage.getItem('Assets.version'));
			console.log('listVersion:', window.localStorage.getItem('Assets.listVersion'));
			console.log('*********************************');
		};


		const updateAssets = (buffer) => {
			console.log(LOG_PREF+'updateAssets');
			for (let file in buffer.list) {
				if (buffer.list.hasOwnProperty(file) === false) {
					continue;
				}
				let lang = 'all';
				if (buffer.list[file]['lang']) {
					lang = buffer.list[file]['lang'];
				}
				if (list[lang][file] && list[lang][file].version) {
					Phonegap.deleteFile('V' + list[lang][file].version + file);
				}
				list[lang][file] = buffer.list[file];
				list[lang][file].version = version;
			}
			version++;
			save();
			saveList();
		};


		const getItems = () => {
			console.log(LOG_PREF+'getItems');
			let allItems = {},
				file;

			for (file in list.all) {
				if (list.all.hasOwnProperty(file) === false) {
					continue;
				}
				allItems[file] = list.all[file];
			}
			for (file in list[Oldies.getLang()]) {
				if (list[Oldies.getLang()].hasOwnProperty(file) === false) {
					continue;
				}
				allItems[file] = list[Oldies.getLang()][file];
			}
			return allItems;
		};


		const getItem = (file) => {
			console.log(LOG_PREF+'getItem');
			let item;

			// Search in list[lang]
			if (Oldies.getLang() && list[Oldies.getLang()]) {
				item = list[Oldies.getLang()][file];
				if (item) {
					return item;
				}
			}
			// Search in list.all
			item = list.all[file];
			if (item) {
				return item;
			}
			// default: return null
			return null;
		};


		const updateVersion = (version) => {
			console.log(LOG_PREF+'updateVersion');
			listVersion[Oldies.getLang()] = version;
			save();
		};


		const getListVersion = () => {
			console.log(LOG_PREF+'getListVersion');
			return listVersion[Oldies.getLang()] ? listVersion[Oldies.getLang()] : listVersion['initial'];
		};


		return {
			get version() { return version; },
			get listVersion() { return listVersion; },

			init: init,
			getItem: getItem,
			getItems: getItems,
			debug: debug,
			getListVersion: getListVersion,
			updateVersion: updateVersion,
			updateAssets: updateAssets,
		};
	})();
};