
import Db       from './Db';
import Phonegap from './Phonegap';
import Updater  from './Updater';

import { BO_URL, LANG } from '../Config';

import TransitionQueuedActions from '../transition/TransitionQueuedActions';


let OPTIONS = {
	http: BO_URL,
	// Value still unknown as it comes from
	// app/views/elements/device_configuration.ctp:	echo 'Env.http="' . $this->params['environment']->http . '";';
	// Protocol switch: http:// or file:// ?
};
export { OPTIONS };


let getUrl = (url, useHttp, absolute) => {

    if (!url) {
        return '';
    }
    if (useHttp === true) {
        if (url[0] !== '/') {
                url = '/' + url;
        }
        return OPTIONS.http + url;
    }
    else {
        if (url[0] === '/') {
            url = url.substr(1);
        }
        if (typeof Phonegap === 'object') {
            url = Phonegap.getUrl(url);
        }
        if (absolute === true && url[0] !== '/' && url.indexOf('://') === -1) {
            let loc = document.location.pathname;
            url = loc.substr(0, loc.lastIndexOf('/') + 1) + url;
        }
        return url;
    }
};
export { getUrl };


let init = (reduxStore) => {
	// defer
	window.setTimeout(() => {
		TransitionQueuedActions.performWhenAvailable(() => {
			Db.initialize(null, reduxStore);

			if (typeof cordova !== 'undefined') {
				Phonegap.initialize(Updater.startUpdate);
			}
		});
	}, 1);
};
export { init };


let getLang = () => {

	// FIXME TEMP DEV !
	return 'fre';


	switch (LANG) {
		case 'fr': return 'fre';
		case 'en': return 'eng';
		default: console.error('unsupported language: '+LANG);
	};
};
export { getLang };


export default {
	OPTIONS: OPTIONS,
	getUrl : getUrl,
	init   : init,
	getLang: getLang,
};
