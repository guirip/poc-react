
import ons           from 'onsenui';

import fetchHelper   from '../../core/util/FetchHelper';

import assetsFactory from './assetsFactory';
import bufferFactory from './bufferFactory';
import Phonegap      from './Phonegap';
import Db            from './Db';
import Oldies        from './Oldies';


const LOG_PREF = '[Updater] ';


export default (() => {

	let assets = null,
		buffer = null,
		inProgress = false,
		_isInitialized = false,
		_forcedCanceled = false,
		_filesToDownload,
		_filesRemoteDatas,
		readyToCommit = false,
		readyToRestart,
		_noSurprisesTimer = null,
		NEW_FILES_URL = 'manifest/phonegap?pixel_ratio=1',
		TIMEOUT = 20 * 1000,
		NO_SURPRISES_TIMEOUT = 1000 * 10 * 60,
		ASSETS_VERSION,
		ASSETS_DEFINITION_FILE;


	// public method for getting the content of an URL if this URL is in the assets
	const getUrl = (url) => {
		console.log(LOG_PREF+'getUrl');
		url = (url[0] === "/") ? url : "/" + url;
		let indexParams = url.lastIndexOf('?');
		if (indexParams !== -1) {
			url = url.substr(0, indexParams);
		}
		// not ready yet
		if (!assets || _isInitialized === false) {
			return null;
		}

		let asset = assets.getItem(url);
		if (!asset || !asset.version) {
			return null;
		}

		// url exist in the assets and is a valid asset
		return Phonegap.getPath("V" + asset.version + url);
	};

	const init = (callback) => {
		console.log(LOG_PREF+'init');
		if (typeof cordova === "undefined") {
			console.error(LOG_PREF+'A Cordova context is needed in order to use Updater');
		}
		inProgress = false;
		_isInitialized = false;
		ASSETS_VERSION = 'global';
		// TODOLATER: why use "js" folder ? Better to use "offline"... but can break apps
		// change also the "assets" task in deploy.rb
		ASSETS_DEFINITION_FILE = 'assets_' + ASSETS_VERSION + '.json';
		buffer = bufferFactory();
		assets = assetsFactory();
		buffer.init();
		assets.init(() => {
			console.log(LOG_PREF+'initialized');
			_isInitialized = true;
			if (callback) {
				callback();
			}
		});
	};

	// what could be: timeout / error / abord / notmodified / parsererror
	const _userInteraction = (when, what) => {
		console.log(LOG_PREF+'_userInteraction '+when);

		if (when === 'remoteCheckUpdate') {
			// MA: maybe not a good idea, disable for now
			//WebApp.ui.display_msg(WebApp.i18n.t('no_connection','updater'), 3000);
		}
		else if (when === 'showCancelUpdate') {
			ons.notification.confirm('Annuler la mise à jour ?', { // TODO i18n
				callback: (status) => {
					if (status) {
						_forcedCanceled = true;
						downloadRemoteEnd();
					}
				},
			});
		}
		else if (when === "updateProgressBar") {
			setProgress(what);
		}
		else if (when === "clearProgress") {
			clearProgress();
		}
		else if (when === "askForUpdate") {
			let size_txt;
			if (what.size < 1024) {
				size_txt = what.size + " octets";
			} else if (what.size < (1024 * 1024)) {
				size_txt = (Math.ceil(what.size / 1024 * 100) / 100) + " Ko";
			} else {
				size_txt = (Math.ceil(what.size / 1024 / 1024 * 100) / 100) + " Mo";
			}

			let confirmResult = ons.notification.confirm(`Effectuer la mise à jour de ${size_txt} ?`, { // TODO i18n
				callback: (status) => {
					if (status) {
						downloadRemoteInit(what);
						// TODO: give ability to cancel update ?
						//_userInteraction('showCancelUpdate');
						downloadRemoteFiles();
					} else {
						inProgress = false;
					}
				},
			});
		}
		else if (when === "restartApplication") {

			// TODO: do not restart app

			//ons.notification.alert('Mise à jour terminée'); // TODO i18n
			console.info('Mise à jour terminée');


			// TODO: NO APP RESTART
			/*
			let restartApp = () => {
				window.location.hash = '';
				// MA: set timestamp for preventing intersticiel to be displayed after updater finished
				window.localStorage.setItem('mbs-reload-from-updater', new Date().getTime());

				//WebApp.PubSub.publish('mbs.application.reload');
			};
			*/
		}
		else if (when === "newAppVersion") {
			ons.notification.alert('Nouvelle version détectée'); // TODO i18n
		}
		else if (when === "timeout") {
			ons.notification.alert('Délai dépassé'); // TODO i18n
		}
	};

	const beginUpdate = (datas) => {
		console.log(LOG_PREF+'beginUpdate');
		_userInteraction('askForUpdate', datas);
	};

	const buildRessourcesInfos = () => {
		console.log(LOG_PREF+'buildRessourcesInfos');

		let allAssets = assets.getItems();
		for (let file in buffer.list) {
			if (buffer.list.hasOwnProperty(file) === false) {
				continue;
			}

			allAssets[file] = buffer.list[file];
		}
		return allAssets;
	};

	const startUpdate = (cbAfterRemoteCheckUpdate) => {
		console.log(LOG_PREF+'startUpdate');
		if (!cbAfterRemoteCheckUpdate) {
			cbAfterRemoteCheckUpdate = beginUpdate;
		}
		if (_isInitialized === false) {
			console.log(LOG_PREF+'startUpdate: please initialize Updater first.');
			return false;
		}

		if (inProgress === true) {
			console.log(LOG_PREF+'startUpdate: already in progress.');
			return false;
		}
		inProgress = true;

		// MA: force inProgress to false after 10 min... because popup could be erased
		clearTimeout(_noSurprisesTimer);
		_noSurprisesTimer = setTimeout(() => {
			inProgress = false;
		}, NO_SURPRISES_TIMEOUT);

		let ressources = buildRessourcesInfos();
		remoteCheckUpdate(ressources, cbAfterRemoteCheckUpdate);

		return true;
	};

	const remoteCheckUpdate = (ressources, callback) => {
		console.log(LOG_PREF+'remoteCheckUpdate');

		let params = {
			db_version: Db.getVersion(),
			files: JSON.stringify(ressources),
			db_schema: Db.getSchema(),
			locale: Oldies.getLang(),
			assets_version: ASSETS_VERSION,
			localVersion: assets.getListVersion()
		};

		// convert params as form data (expected by manifest controller)
		let formData = new FormData();
		for (let key in params) {
			if (params.hasOwnProperty(key) === true) {
				formData.set(key, params[key]);
			}
		}

		fetchHelper(Oldies.getUrl(NEW_FILES_URL + "&force=1&time=" + new Date().getTime(), true), {
				body: formData,
				method: 'POST',
			},
			(datas) => {
				try {
					console.log(LOG_PREF+'nb_files:', datas.nb_files);
					console.log(LOG_PREF+'data:', datas.data);
					console.log(LOG_PREF+'datas:', datas);

					if (datas.NEW_VERSION) {
						_userInteraction('newAppVersion');
						inProgress = false;
					} else {
						if (datas.nb_files === 0 && datas.data === false) {
							assets.updateVersion(datas.version);
							inProgress = false;
						} else {
							if (typeof callback === 'function') {
								callback(datas);
							} else {
								// no callback defined on success
								inProgress = false;
							}
						}
					}
				}
				catch (e) {
					console.error(LOG_PREF+'Updater error while processing incoming datas.', datas);
					inProgress = false;
				}
			},
			(...args) => {
				console.error(LOG_PREF+"remoteCheckUpdate error", args);
				_userInteraction('remoteCheckUpdate', args);
				inProgress = false;
			}
		);
	};

	const downloadRemoteInit = (datas) => {
		console.log(LOG_PREF+'downloadRemoteInit');
		readyToCommit = false;
		_forcedCanceled = false;
		_filesToDownload = [];
		_filesRemoteDatas = datas;
		if (datas.nb_files !== 0) {
			for (let file in datas.files) {
				if (datas.files.hasOwnProperty(file) === false) {
					continue;
				}
				_filesToDownload.push(file);
			}
		}
	};

	const downloadRemoteEnd = () => {
		console.log(LOG_PREF+'downloadRemoteEnd');
		_userInteraction('clearProgress');
		_filesToDownload = [];
		_filesRemoteDatas = [];
		inProgress = false;
	};

	const downloadRemoteFiles = () => {
		console.log(LOG_PREF+'downloadRemoteFiles');
		// if user canceled the update
		if (inProgress === false) {
			console.log(LOG_PREF+'User canceled already, so just quit.');
			return;
		}

		if (_filesToDownload.length === 0) {
			console.log(LOG_PREF+'All files have been downloaded !!');
			// user cannot cancel anymore
			_userInteraction('clearProgress');
			readyToCommit = true;
			readyToRestart = false;
			commitNewVersion();
			return;
		}

		let fileName = _filesToDownload[0];
		let fileDatas = _filesRemoteDatas.files[fileName];

		const win = () => {
			// remove successfully downloaded file
			_filesToDownload.shift();

			let percent = (_filesRemoteDatas.nb_files - _filesToDownload.length) / _filesRemoteDatas.nb_files * 100;
			_userInteraction('updateProgressBar', percent);

			downloadRemoteFiles();
		};
		const fail = (errorType, errorCode) => {
			console.log(LOG_PREF+"Error while downloading file: " + errorType + " => " + errorCode);
			// display a message in all types of error
			if (_forcedCanceled === false) {
				_userInteraction('timeout');
			}
			// for now, quit at the first error
			downloadRemoteEnd();
		};

		buffer.downloadRemoteFile(fileName, fileDatas, win, fail);
	};

	const commitNewVersion = () => {
		console.log(LOG_PREF+'commitNewVersion');
		if (readyToCommit !== true) {
			console.log(LOG_PREF+'Bad context');
			// cancel everything
			downloadRemoteEnd();
			return;
		}

		let hasDataToUpdate = false;
		let hasFilesToUpdate = false;

		const checkIsAllUpdated = () => {
			console.log(LOG_PREF+'checkIsAllUpdated');

			hasDataToUpdate = (_filesRemoteDatas.data !== false);
			hasFilesToUpdate = (_filesRemoteDatas.nb_files > 0);

			let isFinished = !hasDataToUpdate && !hasFilesToUpdate;
			console.log(LOG_PREF+'hasDataToUpdate: ' + hasDataToUpdate + ' / hasFilesToUpdate: ' + hasFilesToUpdate);

			if (isFinished === true) {
				inProgress = false;
				console.log(LOG_PREF+'Update is applied!');
				_userInteraction('restartApplication');
				readyToRestart = true;
			}
		};

		checkIsAllUpdated();

		if (hasFilesToUpdate === true) {
			console.log(LOG_PREF+'updating files!');
			buffer.commitBuffer(assets.version, (success) => {
				console.log(LOG_PREF+'commitBuffer done!', success);
				if (!success) {
					// cancel if error
					downloadRemoteEnd();
				} else {
					assets.updateAssets(buffer);
					_filesRemoteDatas.nb_files = 0;
					checkIsAllUpdated();
				}
			});
		}

		if (hasDataToUpdate === true) {
			console.log(LOG_PREF+'updating db!');
			Db.refresh(() => {
				_filesRemoteDatas.data = false;
				checkIsAllUpdated();
			});
		}

		assets.updateVersion(_filesRemoteDatas.version);
	};

	const initProgress = () => {
		console.log(LOG_PREF+'initProgress');
		// TODO
		/*let $loading = $('.home .loading');
		$('.text', $loading).html('');
		$('.progress', $loading).css({width: '0', opacity: 1}).show();
		$loading.css({opacity: 1}).show();*/
	};

	const setProgress = (percent) => {
		console.log(LOG_PREF+`setProgress ${percent}%`);
		// TODO
		// $('.home .loading .progress').css('width', percent + '%');
	};

	const clearProgress = () => {
		console.log(LOG_PREF+'clearProgress');
		// TODO
		// $('.home .loading').css({opacity: 0}).hide();
		// $('.home .loading .progress').css({opacity: 0}).hide();
	};

	const debug = () => {
		console.log(LOG_PREF+'debug');
		assets.debug();
	};

	const clearLocalStorage = () => {
		console.log(LOG_PREF+'clearLocalStorage');
		window.localStorage.removeItem('Assets.version');
		window.localStorage.removeItem('Assets.listVersion');
	};


	return {
		get _filesRemoteDatas() { return _filesRemoteDatas; },
		get _filesToDownload()  { return _filesToDownload; },
		get _forcedCanceled()   { return _forcedCanceled; },
		get _isInitialized()    { return _isInitialized; },
		get _noSurprisesTimer() { return _noSurprisesTimer; },
		get inProgress()             { return inProgress; },
		get NEW_FILES_URL()          { return NEW_FILES_URL; },
		get TIMEOUT()                { return TIMEOUT; },
		get NO_SURPRISES_TIMEOUT()   { return NO_SURPRISES_TIMEOUT; },
		get readyToCommit()          { return readyToCommit; },
		get readyToRestart()         { return readyToRestart; },
		get assets()                 { return assets; },
		get buffer()                 { return buffer; },
		get ASSETS_DEFINITION_FILE() { return ASSETS_DEFINITION_FILE; },

		_userInteraction      : _userInteraction,
		beginUpdate           : beginUpdate,
		buildRessourcesInfos  : buildRessourcesInfos,
		clearLocalStorage     : clearLocalStorage,
		clearProgress         : clearProgress,
		commitNewVersion      : commitNewVersion,
		downloadRemoteEnd     : downloadRemoteEnd,
		downloadRemoteFiles   : downloadRemoteFiles,
		downloadRemoteInit    : downloadRemoteInit,
		getUrl                : getUrl,
		init                  : init,
		initProgress          : initProgress,
		remoteCheckUpdate     : remoteCheckUpdate,
		setProgress           : setProgress,
		startUpdate           : startUpdate,
		debug                 : debug,
	};

})();