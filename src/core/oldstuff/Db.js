
import * as localforage from 'localforage';

import fetchHelper from '../../core/util/FetchHelper';

import { APP_VERSION } from '../Config';

import Updater from './Updater';
import Oldies  from './Oldies';

import { updateFromDb } from '../../redux/actions';


const LOG_PREF = '[Db] ';
const webAppLoadSessionData = true;

let data = {},
	data_f = {},
	extraData = {},
	reduxStore;


const initialize = (initializeCb, store) => {

	// Allows to dispatch an action when Db is updated
	reduxStore = store;


	const finalize = (finalizeCb) => {
		console.log(LOG_PREF+'finalize');

		if (typeof cordova !== 'undefined' || webAppLoadSessionData === true) {
			loadSessionData(() => {
				build_indexes();
				if (typeof finalizeCb === 'function') {
					finalizeCb();
				}
			});
		} else {
			build_indexes();
			if (typeof finalizeCb === 'function') {
				finalizeCb();
			}
		}
	}

	const getNewVersionOfOfflineData = () => {
		console.log(LOG_PREF+'Getting new version of offline/datas_' + Oldies.getLang());

		const dbKey = 'db_' + Oldies.getLang(),
			url = Oldies.getUrl('offline/datas_' + Oldies.getLang() + '.txt?' + (new Date().getTime()), true);

		fetchHelper(url, null, (json) => {
			localforage.setItem(dbKey, json).then(() => {

				// reset updater version when restarting from scratch
				if (Updater.assets != null) {
					Updater.assets.listVersion[Oldies.getLang()] = json.version;
				}
				data.version = json.version;

				set_from_storage(() => {
					finalize(initializeCb);
				});

			}).catch((err) => {
				console.error(LOG_PREF+'App was unable to store data ' + dbKey, err);
			});
		})
	};


	// TODO spinner
	//WebApp.loader.setMessage(WebApp.i18n.t('datas', 'loading'));


	const dbKey = 'db_' + Oldies.getLang();
	localforage.getItem(dbKey).then((data) => {

		if (data !== null && typeof data !== 'undefined') {
			set_from_storage(() => {
				if (data.version >= APP_VERSION) {
					finalize(initializeCb);
				} else {
					getNewVersionOfOfflineData();
				}
			});
		} else  {
			getNewVersionOfOfflineData();
		}

	}).catch((err) => {
		console.error(LOG_PREF+'App was unable to check presence of stored data ' + dbKey, err);
	});
};

const build_indexes = (collections) => {
	console.log(LOG_PREF+'build_indexes');

	// disabled indexes
	return;

	// for (let plugin in window.Env.autoindex) {
	// 	if (window.Env.autoindex.hasOwnProperty(plugin) === false) {
	// 		continue;
	// 	}
	// 	//console.log(LOG_PREF+"Auto Indexing: ", plugin);
	// 	window.Env.autoindex[plugin](collections);
	// }
};

const set_from_storage = (cb) => {
	console.log(LOG_PREF+'set_from_storage');

	const dbKey = 'db_' + Oldies.getLang();
	localforage.getItem(dbKey).then((db) => {

		if (db === null || typeof db !== 'object') {
			console.error(LOG_PREF+'Invalid DB from localstorage...');
			// remove item & restart
			localforage.removeItem(dbKey).then(
				() => {
					initialize();
				}).catch((err) => {
					console.error(LOG_PREF+'App was unable to remove stored data ' + dbKey, err);
			});
			return;
		}
		data = db;

		dispatchDbUpdateEvent(null);

		if (typeof cb === 'function') {
			cb();
		}
	}).catch((err) => {
		console.error(LOG_PREF+'App was unable to read stored data ' + dbKey, err);
	});
};

const dispatchDbUpdateEvent = (tables) => {
	console.log('redux store dispatch updateFromDb()');
	reduxStore.dispatch(updateFromDb(tables));
};

const refresh = (callback) => {
	console.log(LOG_PREF+'refresh');
	// TODO display message
	//WebApp.loader.setMessage(WebApp.i18n.t('checking', 'loading'));

	const dbKey = 'db_' + Oldies.getLang();
	localforage.getItem(dbKey).then((db) => {

		if (db === null || db === '') {
			initialize(callback);
		} else {
			const url = Oldies.getUrl('manifest/update?version=' + data.version + '&schema=' + data.schema + "&pixel_ratio=1&locale=" + Oldies.getLang(), true);

			fetchHelper(url, null,
				(json) => {
					let new_datas = json;
					let status = '';

					const applyCb = () => {
						console.log(LOG_PREF+'applyCb');
						if (typeof callback === 'function') {
							callback(status);
						}
					};

					// schema changed
					if (new_datas.schema) {
						localforage.setItem(dbKey, '').then(() => {
							initialize(callback);
							// dispatchDbUpdateEvent(); ??
							applyCb();

						}).catch((err) => {
							console.error(LOG_PREF+'App was unable to update stored data ' + dbKey, err);
						});
					}
					// update available
					else if (new_datas.version) {
						apply_increment(new_datas, (updated_tables) => {
							build_indexes(updated_tables);
							status = 'updated';
							dispatchDbUpdateEvent(updated_tables);
							applyCb();
						});
					} else {
						// TODO :
						//WebApp.loader.setMessage(WebApp.i18n.t('data_uptodate', 'loading'));
						status = 'noupdate';
						applyCb();
					}
				});
		}

	}).catch((err) => {
		console.error(LOG_PREF+'App was unable to check presence of stored data ' + dbKey, err);
	});
};


const getVersion = () => {
	console.log(LOG_PREF+'getVersion');
	if (typeof data === 'object' && typeof data.version === 'number') {
		return data.version;
	}
	return 0;
};

const getSchema = () => {
	console.log(LOG_PREF+'getSchema');
	if (typeof data === 'object' && typeof data.schema === 'string') {
		return data.schema;
	}
	return '';
};

const setExtraData = (data) => {
	console.log(LOG_PREF+'setExtraData');
	if (typeof data !== 'object') {
		console.error(LOG_PREF+'Could not set extra data: invalid data.', setExtraData);
		return;
	}
	extraData = Object.assign(extraData, data);
};

const resetExtraData = () => {
	console.log(LOG_PREF+'resetExtraData');
	extraData = {};
};

const getFreshData = (model, fields, id, callback) => {
	console.log(LOG_PREF+'getFreshData');

	fetchHelper(Oldies.getUrl('manifest/refresher', true), {
			body: JSON.stringify({
				model : model,
				fields: fields,
				id    : id,
			})
		},
		(data) => {
			if (!data.data ||  !data.data[model] || typeof data.data[model][id] === 'undefined') {
				console.error(LOG_PREF+'Invalid response for getFreshData', data);
				callback(null);
				return;
			}
			callback(data.data[model][id]);
		},
		() => {
			callback(null);
		},
	);
};


const loadSessionData = (callback) => {
	console.log(LOG_PREF+'loadSessionData');

	fetchHelper(Oldies.getUrl('offline/datas_f_' + Oldies.getLang() + '.txt?' + (new Date().getTime()), true), null,
		(json) => {
			data_f = json;
			setExtraData(data_f);

			if (typeof callback === 'function') {
				callback();
			}
		},
		() => {
			if (typeof callback === 'function') {
				callback();
			}
		}
	);
};

const apply_increment = (increment, cb) => {
	console.log(LOG_PREF+'apply_increment');
	let collections = [];

	for (let collection in increment) {
		if (increment.hasOwnProperty(collection) === false) {
			continue;
		}

		if (collection === 'version') {
			continue;
		}

		collections.push(collection);

		for (let i in increment[collection]) {
			if (increment[collection].hasOwnProperty(i) === false) {
				continue;
			}

			if (increment[collection][i] === null || typeof increment[collection][i] === 'undefined') {
				delete data[collection].data[i];
			} else {
				data[collection].data[increment[collection][i][0]] = increment[collection][i];
			}
		}
	}

	data.version = increment.version;

	const dbKey = 'db_' + Oldies.getLang();
	localforage.setItem(dbKey, data).then(() => {
		if (typeof cb === 'function') {
			cb(collections);
		}
	}).catch((err) => {
		console.error(LOG_PREF+'App was unable to increment version in stored data ' + dbKey, err);
	});
};


/**
 * Check criterias:
 *  - starts with an uppercase
 *  - does not end with a 's'
 * @type {RegExp}
 */
const TYPE_REGEXP = /^[A-Z].*[a-rt-z]$/;

/**
 * Return coresponding table name (by lowercasing and pluralizing this type)
 * @param  {String} type
 */
const getTableNameFromType = (type) => {
	if (typeof type !== 'string' || TYPE_REGEXP.test(type) === false) {
		console.error(LOG_PREF+'(getTableNameFromType) invalid type', type);
	} else {
		return type.toLowerCase() + 's';
	}
};


/*
let get = (collection, id, field) => {
	if (typeof collection === 'undefined') {
		console.error(LOG_PREF+'Trying to get an invalid collection', collection);
		return null;
	}
	if (typeof field === 'undefined' && typeof id === 'undefined') {
		return get_all(collection);
	} else if (typeof field === 'undefined') {
		return get_object(collection, id);
	}

	let idx = data[collection].order.indexOf(field);
	if (idx === -1) {
		return null;
	}

	let item = data[collection].data[id][idx];
	if (item === null) {
		if (typeof extraData[collection] !== 'undefined' && typeof extraData[collection][id] !== 'undefined') {
			item = extraData[collection][id][idx];
		}
	}
	return item;
};

let where = (collectionName, params, filter) => {
	let results = [];

	let col = getCollection(collectionName);
	if (col === null) {
		let col = getExtraCollection(collectionName);
		if (col === null) {
			console.error(LOG_PREF+'DB where: invalid collection', col);
			return [];
		}
	} else {
		let extraCol = getExtraCollection(collectionName);
		if (extraCol !== null) {
			extraCol.data = mergeObjects(col.data, extraCol);
		}
	}

	let indexes = {};
	for (let p in params) {
		if (params.hasOwnProperty(p) === false) {
			continue;
		}
		indexes[p] = col.order.indexOf(p);
		if (indexes[p] === -1) {
			console.error(LOG_PREF+'Error: could not find this attribute', p, col.order);
			return [];
		}
	}

	for (let i in col.data) {
		if (col.data.hasOwnProperty(i) === false) {
			continue;
		}

		let match = true;
		for (let p in params) {
			if (params.hasOwnProperty(p) === false) {
				continue;
			}
			if (params[p] && typeof params[p] === 'object') {
				let has_one = false;
				let has_all = true;
				let and_test = false;

				for (let v = 0; v < params[p].length; v++) {
					if (!and_test && params[p][0] === '&&') {
						and_test = true;
						continue;
					}
					if (match(col.data[i][indexes[p]], params[p][v])) {
						has_one = true;
					} else {
						has_all = false;
					}
				}
				if (!has_one || has_one && and_test && !has_all) {
					match = false;
				}
			} else {
				if (col.data[i] === null) {
					continue;
				}
				if (!match(col.data[i][indexes[p]], params[p])) {
					match = false;
					break;
				}
			}
		}

		if (match === true && col.data[i]) {
			if (typeof filter !== 'undefined') {
				let value = col.data[i][col.order.indexOf(filter)];
				if (value !== -1) {
					if (value !== null) {
						results.push(value);
					} else {
						console.error(LOG_PREF+'Invalid data', value);
					}
				} else {
					console.error(LOG_PREF+'Invalid data', col.data[i]);
				}
			} else {
				let value = get_object(collectionName, i);
				if (value !== null) {
					results.push(value);
				} else {
					console.error(LOG_PREF+'Invalid data', value);
				}
			}
		}
	}

	extend(results);

	return results;
};

let match = (value, reference) => {
	let sub = (value && reference) ? reference.toString().substring(0, 2) : '';
	switch (sub) {
		case '> ':
			return value > reference.split(' ')[1];
			break;
		case '< ':
			return value < reference.split(' ')[1];
			break;
		case '>=':
			return value >= reference.split(' ')[1];
			break;
		case '<=':
			return value <= reference.split(' ')[1];
			break;
		case '!=':
			return value !== reference.split(' ')[1];
			break;
		default:
			return value === reference;
			break;
	}
};

let find_by = (collections, fields, query, isBeginning) => {
	//console.log(LOG_PREF+'find_by: ', collections, fields, query);

	if (typeof query !== 'string') {
		console.error(LOG_PREF+'query is invalid:', query);
		return [];
	}

	isBeginning = (typeof isBeginning === 'boolean') ? isBeginning : query.length < 3;
	fields = fields || ['title'];

	if (typeof fields === 'object' && fields.length > 1) {
		if (isBeginning === true && $.inArray('title', fields) !== -1) {
			// reduce scope in that case when isBeginning is set
			fields = ['title'];
		}
	} else if (typeof fields === 'string') {
		fields = [fields];
	}

	collections = (typeof collections === 'object') ? collections : [collections];

	query = removeAccents(query);
	query = query.replace(new RegExp('[\\\\\\.\\$\\[\\]\\(\\)\\{\\}\\^\\?\\*\\+\\-]', 'g'), function(e) {
		return '\\' + e;
	});

	let regex = isBeginning ? new RegExp('^' + query, 'i') : new RegExp(query, 'i');
	let results = [];

	for (let i = 0; i < collections.length; i++) {
		let collection = collections[i];

		let _collection = getCollection(collection);
		if (_collection === null) {
			_collection = getExtraCollection(collection);
			if (_collection === null) {
				console.error(LOG_PREF+'findby: invalid collection', collection);
				continue;
			} else {
				_collection = {data: _collection};
			}
		}

		let datas = _collection.data;
		let field_index = [];
		let associations = [];

		for (let i = 0; i < fields.length; i++) {
			let uniqueIdsForCollection = [];

			associations[i] = fields[i].split('@');
			if (associations[i].length > 1) {
				if (typeof indexes[fields[i]] === 'undefined') {
					console.log(LOG_PREF+'Missing indexes:', fields[i]);
					continue;
				}
				let db_index = indexes[fields[i]];
				let ids_found = [];

				for (let current_value in db_index) {
					if (db_index.hasOwnProperty(current_value) === false) {
						continue;
					}
					if (removeAccents(current_value).search(regex) > -1 && typeof db_index[current_value][collection] !== 'undefined') {
						for (let j = 0; j < db_index[current_value][collection].length; j++) {
							if ($.inArray(db_index[current_value][collection][j], ids_found) < 0) {
								let object = get_object(collection, db_index[current_value][collection][j]);
								if (object) {
									if (typeof uniqueIdsForCollection[object.id] === 'undefined') {
										uniqueIdsForCollection[object.id] = true;
										results.push(object);
									}
									ids_found.push(db_index[current_value][collection][j]);
								}
							}
						}
					}
				}
			} else {
				field_index[i] = _collection.order.indexOf(fields[i]);
			}
		}

		for (let item_id in datas) {
			if (datas.hasOwnProperty(item_id) === false) {
				continue;
			}
			for (let i = 0; i < fields.length; i++) {
				if (associations[i].length === 1) {

					if (datas[item_id] === null) {
						console.error(LOG_PREF+'data item is null (?)');
						continue;
					}
					if (datas[item_id][field_index[i]] === null || typeof datas[item_id][field_index[i]] === 'undefined') {
						continue;
					}

					if (removeAccents(datas[item_id][field_index[i]]).search(regex) > -1) {
						if (typeof uniqueIdsForCollection[item_id] === 'undefined') {
							uniqueIdsForCollection[item_id] = true;
							results.push(get_object(collection, item_id));
						}
					}
				}
			}
		}
	}

	extend(results);

	results.sort((a, b) => {
		return a.title > b.title ? 1 : a.title < b.title ? -1 : 0;
	});

	return results;
};

let find = (collection, ids) => {
	if (typeof ids === 'undefined') {
		return get(collection);
	}

	ids = (typeof ids === 'object') ? ids : [ids];

	let results = [], res;

	for (let i = 0; i < ids.length; i++) {
		res = get_object(collection, ids[i]);
		if (res !== null) {
			results.push(res);
		}
	}

	extend(results);

	return results;
};

let table_name = (model) => {
	return models[model];
};

// appends some methods to result sets
let extend = (results) => {
	for (let i in ResultSetMethods) {
		if (ResultSetMethods.hasOwnProperty(i) === false) {
			continue;
		}
		results[i] = ResultSetMethods[i];
	}
};

let getCollection = (collection) => {
	if (typeof data[collection] !== 'undefined') {
		switch (collection) {
			case 'events':
			case 'event_categories':
			case 'events_categories':
				// MA : return a copy not a reference !
				return clone(data[collection]);
		}
		return data[collection];
	}
	return null;
};

let getExtraCollection = (collection) => {
	if (typeof extraData[collection] !== 'undefined') {
		switch (collection) {
			case 'events':
			case 'event_categories':
			case 'events_categories':
				// MA : return a copy not a reference !
				return clone(extraData[collection]);
		}
		return extraData[collection];
	}
	return null;
};

let debugTable = function(_table) => {
	if (isEmpty(_table) === true) {
		console.error(LOG_PREF+'Invalid parameter table', table);
		return;
	}
	let data = data[_table].data;
	let order = data[_table].order;

	let infos = [];

	for (let i in order) {
		if (order.hasOwnProperty(i) === false) {
			continue;
		}
		infos[i] = {row: order[i], size: 0, hasData: 0, hasNoData: 0};
	}
	for (let i in data) {
		if (data.hasOwnProperty(i) === false) {
			continue;
		}

		for (let j in order) {
			if (order.hasOwnProperty(j) === false) {
				continue;
			}
			infos[j].size += JSON.stringify(data[i][j]).length;
			if (data[i][j] === null || data[i][j] === '') {
				infos[j].hasNoData++;
			} else {
				infos[j].hasData++;
			}
		}

	}

	infos = infos.sort(function(x, y) {
		if (x.size < y.size) {
			return 1;
		}
		if (x.size > y.size) {
			return -1;
		}
		return 0;
	});

	for (let i = 0; i < infos.length; i++) {
		console.log(LOG_PREF+'row', infos[i].row, 'size', parseInt(infos[i].size / 1024), 'ko', 'has data', infos[i].hasData, 'has no data', infos[i].hasNoData);
	}
};

let debug = function() => {
	let infos = [];
	let totalSize = 0;

	for (let i in data) {
		if (data.hasOwnProperty(i) === false) {
			continue;
		}
		if (typeof data[i].data === 'undefined') {
			continue;
		}
		let dataCpt = WebApp.jsDomTools.objectTotalLength(data[i].data, true);
		if (isEmpty(dataCpt) === true || isNaN(dataCpt) === true || dataCpt < 1) {
			continue;
		}

		let dataSize = JSON.stringify(data[i].data).length;
		totalSize += dataSize;
		if (dataSize < 1024) {
			continue;
		}
		infos.push({table: i, size: dataSize, cpt: dataCpt});
	}

	infos = infos.sort(function(x, y) {
		if (x.size < y.size) {
			return 1;
		}
		if (x.size > y.size) {
			return -1;
		}
		return 0;
	});

	for (let i = 0; i < infos.length; i++) {
		console.log(LOG_PREF+'Table', infos[i].table, 'has', infos[i].cpt, 'elements for a total of', parseInt(infos[i].size / 1024), 'ko', '(', parseInt(infos[i].size / totalSize * 100), '%)');
	}
	console.log(LOG_PREF+'Total size:', parseInt(totalSize / 1024), 'ko');
};



// appends some methods
let ResultSetMethods = {
	order: function(str, naturalSort) {
		if (typeof str !== 'string') {
			console.error(LOG_PREF+'db order: is not valid', str);
			return this;
		}
		let fields = str.split(/,/);
		let field = fields[0].trim();

		let fieldSecondary = null;
		if (fields.length > 1) {
			fieldSecondary = fields[1].trim();
		}

		let compare = function(a, b) {
			if (a === null) {
				console.error(LOG_PREF+'WebApp DB sort: this item is null.');
				return -1;
			}
			if (b === null) {
				console.error(LOG_PREF+'WebApp DB sort: this item is null.');
				return 1;
			}
			if (a.is_all === 1) {
				return -1;
			}
			if (b.is_all === 1) {
				return 1;
			}
			if (a.is_last || b.is_first) {
				return 1;
			}
			if (b.is_last || a.is_first) {
				return -1;
			}

			let __a__ = a[field];
			let __b__ = b[field];

			if (typeof a[field] === 'string') {
				__a__ = (a[field]).toLowerCase();
				__b__ = (b[field]).toLowerCase();
			}

			if (__a__ === __b__) {
				if (fieldSecondary !== null) {
					let __a__ = (a[fieldSecondary] + '').toLowerCase();
					let __b__ = (b[fieldSecondary] + '').toLowerCase();
					return __a__ < __b__ ? -1 : 1;
				}
				return 0;
			}

			if (naturalSort === true) {
				let _a = __a__;
				let _b = __b__;
				let same = -1;
				for (let i = 0, max = _a.length; i < max; i++) {
					if (_a[i] === _b[i]) {
						same = i;
					}
				}
				same++;
				_a = _a.substring(same);
				_b = _b.substring(same);
				let __a = parseInt(_a);
				let __b = parseInt(_b);
				if (!isNaN(__a) && !isNaN(__b)) {
					_a = __a;
					_b = __b;
				} else {
					if (__a__.localeCompare) {
						return __a__.localeCompare(__b__);
					}
				}

				if (_a < _b) {
					return -1;
				}
				if (_a > _b) {
					return 1;
				}
				return 0;
			} else {
				return __a__ < __b__ ? -1 : 1;
			}
		};

		this.sort(compare);
		if (str.split(' ')[1] === 'desc') {
			this.reverse();
		}
		return this;
	},
	concat: function(new_array) {
		for (let i = 0; i < new_array.length; i++) {
			this.push(new_array[i]);
		}

		return this;
	},
	where: function(field, value) {
		for (let i = 0; i < this.length; i++) {
			if (this[i][field] == value) {
				return this[i];
			}
		}
	},
	paginate: function(params) {
		let per_page = params.per_page || 100;
		let page = params.page || 1;
		let start = (page - 1) * per_page;
		let end = start + per_page;
		let subset = [];

		subset.count = this.length;
		subset.per_page = per_page;

		for (let i = start; i < end; i++) {
			if (typeof this[i] !== 'undefined') {
				subset.push(this[i]);
			}
		}

		extend(subset);
		return subset;
	}
};

function get_object(collectionName, id) {
	if (isEmpty(collectionName) === true) {
		console.error(LOG_PREF+'Invalid attribute collection', collectionName);
		return null;
	}
	let obj = {_collection: collectionName};
	let collection = getCollection(collectionName);
	if (collection === null) {
		collection = getExtraCollection(collectionName);
		if (collection === null) {
			console.error(LOG_PREF+'This collection is not defined', collectionName);
			return null;
		} else {
			collection = {data: collection, order: []};
			// Hum extraCollection does not have order fields...
		}
	}

	for (let i = 0; i < collection.order.length; i++) {
		let tmp = collection.data[id];

		if (typeof tmp === 'undefined') {
			if (typeof extraData[collectionName] !== 'undefined' && typeof extraData[collectionName][id] !== 'undefined') {
				tmp = extraData[collectionName][id];
			} else {
				return null;
			}
		}
		// defensive code
		if (tmp === null) {
			return null;
		}

		obj[collection.order[i]] = tmp[i];
	}

	let collectionF = data_f[collectionName];
	if (isEmpty(collectionF) === false) {
		for (let i = 0; i < collectionF.order.length; i++) {
			// do not overwride existing attribute... because must be fresher
			if (typeof obj[collectionF.order[i]] !== 'undefined') {
				continue;
			}
			let tmp = collectionF.data[id];

			if (typeof tmp === 'undefined') {
				if (typeof extraData[collectionName] !== 'undefined' && typeof extraData[collectionName][id] !== 'undefined') {
					tmp = extraData[collectionName][id];
				}
			}
			if (typeof tmp !== 'undefined' && tmp !== null) {
				obj[collectionF.order[i]] = tmp[i];
			}
		}
	}

	for (let i in ObjectsMethods) {
		if (ObjectsMethods.hasOwnProperty(i) === false) {
			continue;
		}
		obj['_' + i] = ObjectsMethods[i];
	}

	return obj;
}

let ObjectsMethods = {
	load: function(assoc) {
		if (typeof this === 'undefined') {
			return null;
		}

		if (indexes[this._collection + '_' + assoc]) {
			let ids = indexes[this._collection + '_' + assoc][this.id] || [];
			let items = [];

			for (let i = 0; i < ids.length; i++) {
				let item = {};
				for (let j = 0; j < data[assoc].order.length; j++) {
					if (typeof data[assoc].data[ids[i]] === 'undefined') {
						continue;
					}
					item[data[assoc].order[j]] = data[assoc].data[ids[i]][j];
				}

				for (let m in ObjectsMethods) {
					if (ObjectsMethods.hasOwnProperty(m) === false) {
						continue;
					}
					item['_' + m] = ObjectsMethods[m];
				}

				items.push(item);
			}

			extend(items);
			this[assoc] = items;
		} else {
			this[assoc] = null;
		}

		return this;
	},
	load_assoc: function(assoc_name, params) {
		if (typeof this === 'undefined') {
			return null;
		}

		let model = this[assoc_name + '_type'], id = this[assoc_name + '_id'], params = params || {};
		if (model && id) {
			this[assoc_name] = get_object(table_name(model), id);
		} else if (id) {
			let collection = params.collection || assoc_name + 's';
			this[assoc_name] = get_object(collection, id);
		} else {
			this[assoc_name] = null;
		}

		return this;
	},
};

function get_all(collection) {
	let items = [];
	let col = getCollection(collection);
	if (col !== null) {
		for (let i in col.data) {
			if (col.data.hasOwnProperty(i) === false) {
				continue;
			}
			if (col.data[i]) {
				items.push(get_object(collection, i));
			}
		}
	}
	return items;
}

function _refresh_models() {
	models = {};

	for (let section in window.Env.searchables) {
		if (window.Env.searchables.hasOwnProperty(section) === false) {
			continue;
		}
		for (let table in window.Env.searchables[section]) {
			if (window.Env.searchables[section].hasOwnProperty(table) === false) {
				continue;
			}
			models[window.Env.searchables[section][table].model] = table;
		}
	}
}
*/


export default {
	get data(){ return data; },
	initialize: initialize,
	getVersion: getVersion,
	getSchema : getSchema,
	refresh   : refresh,
	getTableNameFromType: getTableNameFromType,
	dispatchDbUpdateEvent: dispatchDbUpdateEvent,
};;
