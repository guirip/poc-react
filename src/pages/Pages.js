
import HomePage      from './home/HomePage';
import ListPage      from './list/ListPage';
import MapPage       from './map/MapPage';
import ExhibitorPage from './exhibitor/ExhibitorPage';

const CSSCLASS = 'page-container';
export { CSSCLASS as PAGE_CSSCLASS };


// Pages definition
const tPages = [
	{
		key       : 'Home',
		path      : '/home',
		label	  : 'Accueil', // TODO: i18n
		component : HomePage,
		elId      : 'home-page',
		events    : {},
	},
	{
		key       : 'List',
		path      : '/list',
		label	  : 'Liste', // TODO: i18n
		component : ListPage,
		elId      : 'list-page',
		events    : {},
	},
	{
		key       : 'Map',
		path      : '/map',
		label	  : 'Carte', // TODO: i18n
		component : MapPage,
		elId      : 'map-page',
		mountOnce : true,
		events    : {
			updOptions: 'map-update-options'
		},
	},
	{
		key       : 'Exhibitor',
		path      : '/exhibitor',
		label	  : 'Exposant', // TODO: i18n
		component : ExhibitorPage,
		elId      : 'exhibitor-page',
		inMenu    : false,
		events    : {},
	},
];


// Store container DOM element associated with each page
let elements = {},
	getElement = (elId) => {
		if (!elements[elId]){
			elements[elId] = document.getElementById(elId);
		}
		return elements[elId];
	};

// Assigned to each page
let commonDefaultProperties = {
	getElement: function() {
		return getElement(this.elId);
	},
	setElement: function(el) {
		elements[this.elId] = el;
	},
	isMounted : false,
	active    : false,
};


// Create a key/value association and expose it
let pagesDefinition = {};
tPages.forEach((def) => {
	pagesDefinition[def.key] = Object.assign(def, commonDefaultProperties);
});


export default pagesDefinition;
