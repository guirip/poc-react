import React from 'react';

import ons from 'onsenui';
import { Button } from 'react-onsenui';

import './HomePage.css';

import Router from '../../core/Router';
import Pages from '../Pages';

import Menu from '../../components/menu/Menu';

//const LOG_PREF = '[HomePage] ';


const HomePage = () => {

	let showNotif = () => {
		ons.notification.alert({
			title: 'Very unimportant message',
			message: 'Hello you 😍'
		});
	};

	return (
		<Menu associatedPageKey={Pages.Home.key}>
			<div id="home-content">
				<p>Home is where honey is.</p>
				<Button onClick={() => Router.go(Pages.List, { type:'Service' })}>go 'Service' list</Button>
				<Button onClick={() => Router.go(Pages.List, { type:'Exhibitor' })}>go 'Exhibitor' list</Button>
				<Button onClick={() => Router.go(Pages.Map, {})}>go map</Button>
				<Button onClick={() => showNotif() }>show alert</Button>
				<Button onClick={() => Router.go(Pages.Exhibitor, { id:33 })}>go exhibitor 33</Button>
				<p><small>(swipe from right to display side menu)</small></p>
			</div>
		</Menu>
	);
}

export default HomePage;