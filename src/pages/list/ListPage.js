import React, { PropTypes, Component } from 'react';

import Skeleton          from '../../components/skeleton/Skeleton';
import Menu		         from '../../components/menu/Menu';
import List		         from '../../components/list/List';

import Pages	         from '../Pages';

import Router            from '../../core/Router';

import { setListType }   from '../../redux/actions';


const LOG_PREF = '[ListPage] ';


class ListPage extends Component {

	constructor(props) {
		super(props);
		console.log(LOG_PREF+'constructor', props);

		this.setFromProps(props, true);

		this.state = this.store.getState()[Pages.List.key];

		this.store.subscribe(this.handleStoreEvent.bind(this));
	}

	handleStoreEvent(...args) {
		console.log(LOG_PREF+'handleStoreEvent');
		this.setState(this.store.getState()[Pages.List.key]);
	}

	componentWillReceiveProps(props) {
		console.log(LOG_PREF+'componentWillReceiveProps');
		this.setFromProps(props);
	}

	setFromProps(props, fromConstructor) {
		this.store = props.store;

		// Avoid dispatching a redux action if type has not changed
		let typeHasChanged = this.dataType !== props.type;
		this.dataType = props.type;

		if (typeHasChanged && fromConstructor !== true) {
			this.dispatchListTypeUpdate();
		}
	}

	componentWillMount() {
		this.dispatchListTypeUpdate();
	}

	dispatchListTypeUpdate() {
		console.log(LOG_PREF+'dispatch type');
		this.store.dispatch(setListType(this.dataType));
	}

	getHelpers() {
		switch (this.state.type) {
			case 'Service': return {
					"data-id"         : row => String(row[0]),
					"data-original-id": row => String(row[1]),
					"data-type"       : ()  => this.state.type,
					text              : row => String(row[1]),
					textMinor         : row => String(row[2]),
					onClick           : () => (event) => {
						Router.go(Pages.Map, {
							poiId  : event.target.dataset.originalId,
							poiType: event.target.dataset.type,
						});
					},
				};

			case 'Exhibitor': return {
					"data-id"         : row => String(row[0]),
					text              : row => String(row[1]),
					className         : row => {
						if (row[18]) {
							const lump = JSON.parse(row[18]);
							if (lump.bold === '1') {
								return 'emphasised';
							}
						}
						return '';
					},
					onClick           : () => (event) => {
						Router.go(Pages.Exhibitor, {
							id: event.target.dataset.id,
						});
					},
				};

			default: console.error(`Type ${this.state.type} is not handled yet`);
		}
	}


	render() {
		console.log(LOG_PREF+'render');

		let listComponent;
		if (this.state && this.state.items) {
			listComponent = <List items={this.state && this.state.items} helpers={this.getHelpers()} type={this.state && this.state.type} />;
		} else {
			listComponent = <List />;
		}

		return (
			<Menu associatedPageKey={Pages.List.key}>
				<Skeleton title={`${Pages.List.label} [${this.state.type}]`} pageKey={Pages.List.key}>
					{listComponent}
				</Skeleton>
			</Menu>
		);
	}

}
ListPage.propTypes = {
	type: PropTypes.string.isRequired,
}

export default ListPage;
