import React, { PropTypes, Component } from 'react';

import { List, ListItem, ProgressCircular } from 'react-onsenui';

import './ExhibitorPage.css';

import Skeleton from '../../components/skeleton/Skeleton';
import Menu		from '../../components/menu/Menu';
import Pages	from '../Pages';

import Db	    from '../../core/oldstuff/Db';
import Oldies   from '../../core/oldstuff/Oldies';

import { setExhibitorId } from '../../redux/actions';


const LOG_PREF = '[ExhibitorPage] ';


class ExhibitorPage extends Component {

	constructor(props) {
		super(props);
		console.log(LOG_PREF+'constructor', props);

		this.setFromProps(props, true);

		this.state = this.store.getState()[Pages.Exhibitor.key];

		this.store.subscribe(this.handleStoreEvent.bind(this));
	}

	handleStoreEvent(...args) {
		console.log(LOG_PREF+'handleStoreEvent');
		this.setState(this.store.getState()[Pages.Exhibitor.key]);
	}

	componentWillReceiveProps(props) {
		console.log(LOG_PREF+'componentWillReceiveProps');
		this.setFromProps(props);
	}

	setFromProps(props, fromConstructor) {
		this.store = props.store;

		// Avoid dispatching a redux action if id has not changed
		let idHasChanged = this.exhibitorId !== props.id;
		this.exhibitorId = props.id;

		if (idHasChanged && fromConstructor !== true) {
			this.dispatchExhibitorIdUpdate();
		}
	}

	componentWillMount() {
		this.dispatchExhibitorIdUpdate();
	}

	dispatchExhibitorIdUpdate() {
		console.log(LOG_PREF+'dispatch id');
		this.store.dispatch(setExhibitorId(this.exhibitorId));
	}

	shouldComponentUpdate(nextProps, nextState) {
		return this.exhibitorId !== nextProps.id || JSON.stringify(this.state) !== JSON.stringify(nextState);
	}

	/*
	0:  "id"                  ex: 33
	1:  "company"             ex: "ACRDM"
	2:  "description"         ex: "desc"
	3:  "address"             ex: "90 route de Tarbes"
	4:  "postal_code"         ex: "31170"
	5:  "city"                ex: "Tournefeuille"
	6:  "phone"               ex: "+33 6 73 62 42 00"
	7:  "email"               ex: "cedric.bellot@acrdm.fr"
	8:  "website"             ex: "http://www.acrdm.fr"
	9:  "country_id"          ex: 35
	10: "logo_file_name"      ex: ""
	11: "contact_title"       ex: ""
	12: "contact_firstname"   ex: ""
	13: "contact_lastname"    ex: ""
	14: "contact_email"       ex: ""
	15: "contact_position"    ex: ""
	16: "activity"            ex: ""
	17: "brands"              ex: ""
	18: "lump"                ex: "{"bold":null,"direct":"0","color":"#000000","agent_countries":"","contacts":[{"n":"M. Cédric Bellot, PDG"}],"entities":[]}"
	 */

	getCountry(countryId) {
		return Db.data.countries.data[countryId][1];
	}

	getDetail() {
		if (!this.state || !this.state.exhibitor) {

			return (
				<List className="loader">
					<li>
						<ProgressCircular indeterminate></ProgressCircular>
					</li>
				</List>
			);
		} else {
			return (
				<div>
					<List>
						<ListItem modifier="tappable">
							<div className="prop-desc">
								{this.state.exhibitor[3]} - {this.state.exhibitor[4]} {this.state.exhibitor[5]} - {this.getCountry(this.state.exhibitor[9])}
							</div>
						</ListItem>

						{ this.state.exhibitor[6] &&
							<ListItem modifier="tappable">
								<div className="prop-desc">
									<ons-icon icon="fa-phone"></ons-icon>
									{this.state.exhibitor[6]}
								</div>
							</ListItem>
						}

						{ this.state.exhibitor[7] &&
							<ListItem modifier="tappable">
								<div className="prop-desc">
									<ons-icon icon="fa-envelope"></ons-icon>
									{this.state.exhibitor[7]}
								</div>
							</ListItem>
						}

						{ this.state.exhibitor[8] &&
							<ListItem modifier="tappable">
								<div className="prop-desc">
									<ons-icon icon="fa-link"></ons-icon>
									{this.state.exhibitor[8]}
								</div>
							</ListItem>
						}

					</List>

					{ this.state.exhibitor[2] &&
						<div className="description">
							<small>Description</small>

							<div>{this.state.exhibitor[2]}</div>

							{ this.state.exhibitor[10] &&
								<img src={ Oldies.getUrl(this.state.exhibitor[10]) } alt="" />
							}


							<img src={ Oldies.getUrl('/assets/aircrafts/carbon-400.jpg') } alt="" />
						</div>
					}
				</div>
			);
		}
	}

	render() {
		console.log(LOG_PREF+'render');
		let title = Pages.Exhibitor.label + (this.state.exhibitor && this.state.exhibitor[1] ? ' ' + this.state.exhibitor[1] : '');
		return (
			<Menu associatedPageKey={Pages.Exhibitor.key}>
				<Skeleton title={title} pageKey={Pages.Exhibitor.key}>
					{ this.getDetail() }
				</Skeleton>
			</Menu>
		);
	}
};

ExhibitorPage.propTypes = {
	id: PropTypes.number.isRequired,
	store: PropTypes.object.isRequired,
}

export default ExhibitorPage