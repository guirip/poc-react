import React, { Component } from 'react';

import { Fab, Icon } from 'react-onsenui';

import './MapPage.css';

import Pages  from '../Pages';
import Menu, { toggleMenu } from '../../components/menu/Menu';

const LOG_PREF = '[MapPage] ';


class MapPage extends Component {

	constructor(props) {
		super(props);

		console.log(LOG_PREF+'MapPage props:');
		console.log(LOG_PREF+props);

		/**
		 * @see ../../README.md#using-global-variables
		 */
		this.MobiGeo = window.MobiGeo;
		//this.MobiGeo.setLogLevel('DEBUG');

		this.containerId	 = 'map-container';
		this.isMapLoaded	 = false;
		this.isMapReady		 = false;
		this.areEventsBinded = false;
		this.queuedActions	 = [];

		this.state = {};
	}

	onEvent = (e) => {
		switch (e.type) {
			case Pages.Map.events.updOptions:
				this.state = {
					poiId   : e.data.poiId,
					poiType : e.data.poiType,
				};
				this.applyOptions();
				break;

			default:
				console.warn(LOG_PREF+'Unhandled event ' + e.type);
		}
	};

	bindEventHandlers() {
		this.MobiGeo.Map.on('ready', () => {
			this.isMapReady = true;
			console.log(LOG_PREF+'Map has successfully been loaded');
			if (this.queuedActions.length > 0) {
				this.queuedActions.pop()();
			}
		});

		this.MobiGeo.Map.POI.on('tap', (data) => {
			console.log(LOG_PREF+'POI selected', data);
		});

		let commonErrorHandler = (error) => {
			console.error(LOG_PREF+'MobiGeo encountered an error: ', error);
		};
		this.MobiGeo.on('error', commonErrorHandler);
		this.MobiGeo.Map.on('error', commonErrorHandler);
		this.MobiGeo.Map.Route.on('error', commonErrorHandler);

		this.areEventsBinded = true;
	};

	loadDataset() {
		if (!this.areEventsBinded) {
			this.bindEventHandlers();
		}
		if (!this.isMapLoaded && this.MobiGeo.load('MeL8ooso') === true) {
			this.MobiGeo.Map.create(document.getElementById(this.containerId), { showMapTitle: false });
			this.isMapLoaded = true;
		}
		this.applyOptions();
	};

	/**
	 * Apply options from querystring (e.g poiId to show)
	 */
	applyOptions() {
		let action;

		// Show a POI
		if (typeof this.state.poiId !== 'undefined') {
			let poiId = this.state.poiId;
			action = () => {
				this.MobiGeo.Map.POI.show(poiId, this.state.poiType);
			};
		}

		// Perform the action, or queue it if mobigeo is not ready yet
		if (typeof action === 'function') {
			if (this.isMapReady) {
				action();
			} else {
				this.queuedActions.push(action);
			}
		}
	};

	shouldComponentUpdate() {
		console.log(LOG_PREF+'Never redraw this component as it would load the map from scratch');
		return false;
	}

	componentDidMount() {
		console.log(LOG_PREF+'did mount');
		this.loadDataset();
	}

	render() {
		let menuOpts = {
			swipeable: false,
		};
		return (
			<Menu options={menuOpts} associatedPageKey={Pages.Map.key}>
				<div id={this.containerId}></div>
				<Fab ripple position="top right" onClick={() => toggleMenu(Pages.Map.key) }>
					<Icon icon="md-menu"></Icon>
				</Fab>
			</Menu>
		);
	}
}

export default MapPage;
