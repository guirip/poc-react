import ons from 'onsenui';

import { createStore } from 'redux';
import app             from './redux/reducers';

import Router from './core/Router';

import Oldies from './core/oldstuff/Oldies';

import './index.css';

// @see http://stackoverflow.com/questions/34133808/webpack-ots-parsing-error-loading-fonts?answertab=votes#tab-top
import './lib/css/onsenui.css';
import './lib/css/onsen-css-components.css';
import './lib/css/font_awesome/css/font-awesome.min.css';
import './lib/css/ionicons/css/ionicons.min.css';
import './lib/css/material-design-iconic-font/css/material-design-iconic-font.min.css';



const isCordovaContext = typeof cordova === 'object';

// useful ?
document.addEventListener(isCordovaContext ? 'deviceready' : 'DOMContentLoaded', () => {

	let reduxStore = createStore(app);

	Oldies.init(reduxStore);

	Router.setReduxStore(reduxStore);
	Router.applyCurrentUrl();


	// if running inside a Cordova app
	if (isCordovaContext) {
		ons.setDefaultDeviceBackButtonListener((e) => {
			Router.goBack();
		}, false);
	}
});

